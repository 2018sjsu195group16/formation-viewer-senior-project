﻿using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    public GameObject ARControllerObject;

    public GameObject CoordinatePrefab;
    public GameObject LocationPrefab;
    private string trackedCoordinateId;
    private string previousTrackedCoordinateId;
    private bool hasStartedShowingFormation = false;
    private bool hasFinishedPostCalibration = false;
    private bool hasLoadedFormation = false;
    private static Vector3 referenceOffset = Vector3.zero;

    public enum ViewMode { ArViewMode, TopDownViewMode };
    private ViewMode currentViewMode;
    private ViewMode previousViewMode;
    private bool shouldUpdateViewMode = true;
    public Camera ArCamera;
    public GameObject TopDownCamera;
    public Canvas HelpCanvas;
    public Image LocationImage;
    public GameObject planeGenerator;
    private DetectedPlaneVisualizer detectedPlane;
    private DeviceOrientation previousDeviceOrientation = DeviceOrientation.Portrait;

    private List<Dictionary<string, Coordinate>> formationList;
    private int previousFormationIndex;
    private int formationIndex;
    private bool estimateLight = true;
    private bool shouldShowNextPosition = true;
    private bool shouldShowModels = true;
    private bool shouldShowSpMarker = true;

    private static GameObject fakeAnchor;

    void Awake()
    {
        // Add this controller's ReceiveRnMessage function to the Message Manager
        UnityMessageManager.Instance.OnRNMessage += ReceiveRnMessage;
    }

    void onDestroy()
    {
        // Remove this controller's ReceiveRnMessage function from the Message Manager
        UnityMessageManager.Instance.OnRNMessage -= ReceiveRnMessage;
    }

    /// Start is called before the first frame update
    /// Reference:
    /// https://answers.unity.com/questions/563786/class-constructor-on-instantiated-objects.html
    /// https://docs.unity3d.com/Manual/InstantiatingPrefabs.html
    void Start()
    {
        // Testing purposes only to not have to use React to get file
        //_loadFromFile();

        Debug.Log("Starting Unity FV");

        // Set up fake anchor for when haven't calibrated yet
        fakeAnchor = new GameObject();
        fakeAnchor.transform.position = referenceOffset;

        // Start in Top-Down View Mode
        SetViewMode(ViewMode.TopDownViewMode);
        TopDownCamera.GetComponentInChildren<TopDownView>().SetViewMode(currentViewMode);
        ARControllerObject.GetComponentInChildren<ARController>().SetViewMode(currentViewMode);

        // Perform initial Top-Down View Mode operations before Update can be called
        ArCamera.enabled = false;
        TopDownCamera.GetComponentInChildren<Camera>().enabled = true;
        HelpCanvas.gameObject.SetActive(false);

        UnityMessageManager.Instance.SendMessageToRN(new UnityMessage()
        {
            name = "request formation list",
            callBack = (data) =>
            {
                // Get file JSON from React
                JObject messageData = (JObject)data;

                LoadFormation(messageData["jsonString"].Value<string>());

                // Set initial tracked coordinate ID.
                trackedCoordinateId = messageData["coordinateId"].Value<string>();

                previousTrackedCoordinateId = trackedCoordinateId;
                previousFormationIndex = formationIndex;
            }
        });

        UnityMessageManager.Instance.SendMessageToRN(new UnityMessage()
        {
            name = "request location",
            callBack = (data) =>
            {
                // Get file JSON from React
                JObject messageData = (JObject)data;

                LoadLocation(messageData["locationId"].Value<string>());
            }
        });
    }

    private void _loadFromFile()
    {
        string testFormationPath = Path.GetFullPath(Path.Combine(Path.Combine(Application.dataPath, "dev-assets"), "large-sample-formation-list.json"));
        LoadFormation(File.ReadAllText(testFormationPath));
        LoadLocation("basketball");
        trackedCoordinateId = "";
        previousTrackedCoordinateId = trackedCoordinateId;
        previousFormationIndex = formationIndex;
    }

    void LoadLocation(string locationId)
    {
        TopDownView.LocationType locationType;

        switch (locationId)
        {
            case "football":
                locationType = TopDownView.LocationType.Football;
                break;
            case "basketball":
                locationType = TopDownView.LocationType.Basketball;
                break;
            default:
                throw new System.Exception("method LoadLocation: parameter locationId has an invalid value: " + locationId);
        }

        TopDownCamera.GetComponentInChildren<TopDownView>().SetLocationType(locationType);
    }

    void LoadFormation(string jsonText)
    {
        JObject fileJson = JObject.Parse(jsonText);

        /*
            m.GetValue("id").Value<int>(),
            m.GetValue("seq").Value<string>(),
            m.GetValue("name").Value<string>(),
            m.GetValue("data")
        */

        // Create new empty formation list
        formationList = new List<Dictionary<string, Coordinate>>();
        formationIndex = 0;

        JArray formationsJson = (JArray)fileJson["formations"];

        // Assuming formation pages are in order of array
        foreach (JObject formationJson in formationsJson.Children<JObject>())
        {
            // Create formation
            Dictionary<string, Coordinate> formation = new Dictionary<string, Coordinate>();

            // Title is used in callback to tell React which page we are on (could also use index).
            string formationTitle = formationJson["title"].Value<string>();
            JArray coordinatesJson = (JArray)formationJson.GetValue("coordinates");

            // Create all coordinates and add to formation dictionary
            foreach (JObject coordinateJson in coordinatesJson.Children<JObject>())
            {
                string coordinateId = coordinateJson["id"].Value<string>();
                Coordinate c = Instantiate(CoordinatePrefab).GetComponent<Coordinate>();

                c.Initialize(
                    coordinateId,
                    Coordinate.ModelType.Person,
                    new Vector3(
                        coordinateJson["position"]["x"].Value<float>(),
                        coordinateJson["position"]["y"].Value<float>(),
                        coordinateJson["position"]["z"].Value<float>()
                    ),
                    new Vector3(
                        coordinateJson["orientation"]["x"].Value<float>(),
                        coordinateJson["orientation"]["y"].Value<float>(),
                        coordinateJson["orientation"]["z"].Value<float>()
                    ),
                    true
                );
                c.gameObject.SetActive(false);
                formation.Add(c.GetCoordinateId(), c);
            }

            formationList.Add(formation);
        }
        hasLoadedFormation = true;
    }

    // Track new coordinate ID
    void SetActiveCoordinateId(string coordinateId)
    {
        trackedCoordinateId = coordinateId;
    }

    // TODO Not currently used since most of script needs to be made static
    public ViewMode GetCurrentViewMode()
    {
        return currentViewMode;
    }

    // Change formation
    void SetFormationIndex(int index)
    {
        formationIndex = index;
    }

    void SetReferencePoint(Vector3 refPoint)
    {
        referenceOffset = refPoint;
    }

    void SetShowNextPosition(bool shouldShow)
    {
        shouldShowNextPosition = shouldShow;
    }

    void SetViewMode(ViewMode viewMode)
    {
        currentViewMode = viewMode;
    }

    // Receives messages from RN
    void ReceiveRnMessage(MessageHandler message)
    {
        // message name treated as a type
        string messageType = message.name;
        JObject messageData = message.getData<JObject>();

        switch (messageType)
        {
            case "set formation":
                SetFormationIndex(messageData["formationIndex"].Value<int>());
                break;
            case "set show models":
                shouldShowModels = messageData["shouldShowModels"].Value<bool>();
                break;
            case "set coordinate ID":
                SetActiveCoordinateId(messageData["coordinateId"].Value<string>());
                break;
            case "set show SP marker":
                shouldShowSpMarker = messageData["shouldShowSpMarker"].Value<bool>();
                break;
            case "set reference point type":
                // -1 type means using coordinate ID as reference from current page
                if (messageData["referencePointType"].Value<int>() == -1)
                {
                    // TODO error checking
                    SetReferencePoint(formationList[formationIndex][trackedCoordinateId].GetPosition());
                }
                else
                {
                    SetReferencePoint(
                        TopDownCamera.GetComponentInChildren<TopDownView>().GetReferencePoint(
                            (Location.ReferencePoint)messageData["referencePointType"].Value<int>()
                        )
                    );
                }
                break;
            case "set show next position":
                SetShowNextPosition(messageData["shouldShowNextPosition"].Value<bool>());
                break;
            case "set view mode":
                SetViewMode((ViewMode)messageData["viewMode"].Value<int>());
                break;
            case "set reference point":
                SetReferencePoint(new Vector3(
                    messageData["referencePoint"]["x"].Value<float>(),
                    messageData["referencePoint"]["y"].Value<float>(),
                    messageData["referencePoint"]["z"].Value<float>()
                ));
                break;
            case "set location":
                LoadLocation(messageData["locationId"].Value<string>());
                break;
            default:
                Debug.Log("Unsupported Message from React\n");
                Debug.Log("message name: " + messageType + " ");
                Debug.Log("message data: " + messageData.ToString() + "\n");
                break;
        }

        // send callback
        message.send(new { CallbackTest = "I am Unity callback" });
    }

    // Update is called once per frame
    void Update()
    {
        // If no formation loaded, do nothing
        if (!hasLoadedFormation)
        {
            return;
        }

        if (!hasStartedShowingFormation)
        {
            if (formationList[formationIndex].ContainsKey(trackedCoordinateId))
            {
                // Change tracked coordinate ID to be the current target rather than person.
                formationList[formationIndex][trackedCoordinateId].ChangeModel(Coordinate.ModelType.CurrentTarget);

                if (formationIndex < formationList.Count - 1)
                {
                    // Change tracked coordinate ID of next position to be the next target rather than person.
                    formationList[formationIndex + 1][trackedCoordinateId].ChangeModel(Coordinate.ModelType.NextTarget);
                }

                previousTrackedCoordinateId = trackedCoordinateId;
            }

            // Show formation
            SetFormationActive(shouldShowModels);

            hasStartedShowingFormation = true;
        }

        // Run only if Calibration has completed. 
        if (ARController.getArStatus() == ARController.Status.Running)
        {
            // Perform post calibration unless already complete.
            if (!hasFinishedPostCalibration)
            {
                UnityMessageManager.Instance.SendMessageToRN(new UnityMessage()
                {
                    name = "calibration complete",
                });

                hasFinishedPostCalibration = true;
            }
            else
            {
                // Show/hide start position marker
                ARController.SetSpActive(shouldShowSpMarker);
            }
        }

        // If formation index is not valid, print error and done with Update.
        if (formationIndex < 0 || formationIndex >= formationList.Count)
        {
            Debug.LogError("method Update: formation index is out of bounds of formation list: " + formationIndex, this);
            return;
        }

        // If tracked coordinate ID does not exists in formation, print error and done with Update.
        if (!formationList[formationIndex].ContainsKey(trackedCoordinateId))
        {
            if (trackedCoordinateId != "")
            {
                Debug.LogError("method Update: tracked coordinate ID does not exist: " + trackedCoordinateId, this);
                return;
            }
        }

        // if formation index has changed, show those models instead of current ones
        if (previousFormationIndex != formationIndex)
        {
            // Set previous models inactive
            SetFormationActive(previousFormationIndex, false);

            if (formationList[previousFormationIndex].ContainsKey(trackedCoordinateId))
            {
                // Change previous formation's tracked coordinate ID to be a person rather than current target.
                formationList[previousFormationIndex][trackedCoordinateId].ChangeModel(Coordinate.ModelType.Person);

                if (previousFormationIndex < formationList.Count - 1)
                {
                    // Change previous formation's tracked coordinate ID of next position to be a person rather than next target.
                    formationList[previousFormationIndex + 1][trackedCoordinateId].ChangeModel(Coordinate.ModelType.Person);
                }
            }

            if (formationList[formationIndex].ContainsKey(trackedCoordinateId))
            {
                // Change current formation's tracked coordinate ID to be the current target rather than person.
                formationList[formationIndex][trackedCoordinateId].ChangeModel(Coordinate.ModelType.CurrentTarget);

                if (formationIndex < formationList.Count - 1)
                {
                    // Change tracked coordinate ID of next position to be the next target rather than person.
                    formationList[formationIndex + 1][trackedCoordinateId].ChangeModel(Coordinate.ModelType.NextTarget);
                }
            }

            // Set current models active
            SetFormationActive(shouldShowModels);

            previousFormationIndex = formationIndex;
            shouldUpdateViewMode = true;
        }

        // If tracked coordinate ID has changed, change models on coordinates.
        if (previousTrackedCoordinateId != trackedCoordinateId)
        {
            if (formationList[formationIndex].ContainsKey(previousTrackedCoordinateId))
            {
                // Change previous tracked coordinate ID to be a person rather than current target.
                formationList[formationIndex][previousTrackedCoordinateId].ChangeModel(Coordinate.ModelType.Person);

                if (formationIndex < formationList.Count - 1)
                {
                    // Change previous tracked coordinate ID's next position to be a person rather than next target.
                    formationList[formationIndex + 1][previousTrackedCoordinateId].ChangeModel(Coordinate.ModelType.Person);
                    // Hide previous tracked coordinate ID's next position
                    formationList[formationIndex + 1][previousTrackedCoordinateId].gameObject.SetActive(false);
                }
            }

            if (formationList[formationIndex].ContainsKey(trackedCoordinateId))
            {
                // Change tracked coordinate ID to be the current target rather than person.
                formationList[formationIndex][trackedCoordinateId].ChangeModel(Coordinate.ModelType.CurrentTarget);

                if (formationIndex < formationList.Count - 1)
                {
                    // Change tracked coordinate ID of next position to be the next target rather than person.
                    formationList[formationIndex + 1][trackedCoordinateId].ChangeModel(Coordinate.ModelType.NextTarget);
                }
            }

            // Update previous tracked coordinate ID.
            previousTrackedCoordinateId = trackedCoordinateId;
            shouldUpdateViewMode = true;
        }

        // Show/hide models
        switch (currentViewMode)
        {
            case ViewMode.ArViewMode:
                // If haven't calibrated, no models in AR View
                SetFormationActive(shouldShowModels && hasFinishedPostCalibration);
                break;
            case ViewMode.TopDownViewMode:
                SetFormationActive(shouldShowModels);
                break;
            default:
                throw new System.Exception("method Update: parameter viewMode has an invalid value: " + currentViewMode);
        }

        // Get detected plane if exists and don't have it already
        DetectedPlaneVisualizer plane = planeGenerator.GetComponentInChildren<DetectedPlaneVisualizer>();
        if (detectedPlane == null && plane != null)
        {
            detectedPlane = plane;
        }

        // If anything changes, make each Coordinate update as necessary to the current view mode
        if (currentViewMode != previousViewMode || shouldUpdateViewMode)
        {
            ARControllerObject.GetComponentInChildren<ARController>().SetViewMode(currentViewMode);
            TopDownCamera.GetComponentInChildren<TopDownView>().SetViewMode(currentViewMode);

            switch (currentViewMode)
            {
                case ViewMode.ArViewMode:
                    estimateLight = true;
                    ArCamera.enabled = true;
                    TopDownCamera.GetComponentInChildren<Camera>().enabled = false;
                    HelpCanvas.gameObject.SetActive(true);
                    planeGenerator.GetComponentInChildren<DetectedPlaneGenerator>().SetCanDetectPlanes(true);
                    if (detectedPlane != null)
                    {
                        // Show detected plane in AR View
                        detectedPlane.gameObject.SetActive(true);
                    }
                    break;
                case ViewMode.TopDownViewMode:
                    estimateLight = false;
                    ArCamera.enabled = false;
                    TopDownCamera.GetComponentInChildren<Camera>().enabled = true;
                    HelpCanvas.gameObject.SetActive(false);
                    planeGenerator.GetComponentInChildren<DetectedPlaneGenerator>().SetCanDetectPlanes(false);
                    if (detectedPlane != null)
                    {
                        // Hide detected plane in Top-Down View
                        detectedPlane.gameObject.SetActive(false);
                    }
                    break;
                default:
                    throw new System.Exception("method Update: parameter viewMode has an invalid value: " + currentViewMode);
            }

            // Update current formation's coordinates with current view mdoe
            foreach (KeyValuePair<string, Coordinate> entry in formationList[formationIndex])
            {
                entry.Value.SetViewMode(currentViewMode);
                entry.Value.enableLightEstimation(estimateLight);
            }

            // Set View Mode on next target if it exists
            if (formationList[formationIndex].ContainsKey(trackedCoordinateId) && formationIndex < formationList.Count - 1)
            {
                formationList[formationIndex + 1][trackedCoordinateId].SetViewMode(currentViewMode);
                formationList[formationIndex + 1][trackedCoordinateId].enableLightEstimation(estimateLight);

                if (!hasFinishedPostCalibration)
                {
                    switch (currentViewMode)
                    {
                        case ViewMode.ArViewMode:
                            formationList[formationIndex + 1][trackedCoordinateId].gameObject.SetActive(false);
                            break;
                        case ViewMode.TopDownViewMode:
                            formationList[formationIndex + 1][trackedCoordinateId].gameObject.SetActive(shouldShowNextPosition && shouldShowModels);
                            break;
                        default:
                            throw new System.Exception("method Update: parameter viewMode has an invalid value: " + currentViewMode);
                    }
                }
            }

            previousViewMode = currentViewMode;
            shouldUpdateViewMode = false;
        }

        // Change Top-Down Camera values if device orientation changes (ignores FaceUp, FaceDown, and Unknown orientations)
        DeviceOrientation orientation = Input.deviceOrientation;
        if (previousDeviceOrientation != orientation
            && !(orientation == DeviceOrientation.FaceUp || orientation == DeviceOrientation.FaceDown || orientation == DeviceOrientation.Unknown))
        {
            TopDownCamera.GetComponentInChildren<TopDownView>().SetDeviceOrientation(orientation);
            previousDeviceOrientation = orientation;
        }
    }

    // Set a given formation, according to index, to be active or not
    public void SetFormationActive(int index, bool shouldShow)
    {
        foreach (KeyValuePair<string, Coordinate> entry in formationList[index])
        {
            entry.Value.gameObject.SetActive(shouldShow);
        }

        // Show/hide next position
        if (index + 1 > 0 && index + 1 < formationList.Count && formationList[index + 1].ContainsKey(trackedCoordinateId))
        {
            formationList[index + 1][trackedCoordinateId].gameObject.SetActive(shouldShowNextPosition && shouldShow);
        }
    }

    // Sets current formation to be active or not
    public void SetFormationActive(bool shouldShow)
    {
        SetFormationActive(formationIndex, shouldShow);
    }

    /// <summary>
    /// Transforms a given GameObject to have its position updated to be located
    /// at an offset from the anchor's position.
    /// </summary>
    public static void TransformByAnchorSpace(GameObject objToTransform, Vector3 positionOffset, Vector3 rotationOffset)
    {
        GameObject anchor = fakeAnchor;


        if (ARController.getArStatus() == ARController.Status.Running)
        {
            // Has real AR anchor, so set object transform off of real anchor
            anchor = ARController.anchor.gameObject;
        }

        // Save anchor's and object's local scale
        Vector3 anchorScale = anchor.transform.localScale;

        // Set anchor to normal scale before transformation
        anchor.transform.localScale = Vector3.one;

        objToTransform.transform.SetPositionAndRotation(
            anchor.transform.TransformPoint(positionOffset - referenceOffset),
            anchor.transform.rotation
        );

        // Restore anchor's scale
        anchor.transform.localScale = anchorScale;

        // Rotate object to point in it's proper direction
        objToTransform.transform.Rotate(rotationOffset);
    }

    /// <summary>
    /// Transforms a given GameObject to have its position updated to be located
    /// at an offset from the anchor's position while adjusting for the object's
    /// render height.
    /// </summary>
    public static void TransformByAnchorSpaceHeightAdjusted(GameObject objToTransform, Vector3 offset, Vector3 rotationOffset)
    {
        Renderer renderer = objToTransform.GetComponentInChildren<Renderer>();
        if (renderer == null)
        {
            Debug.Log("renderer cannot be found for GameObject " + objToTransform.name);
        }
        // Update object to be on top of anchor
        Vector3 newOffset = offset;
        newOffset.y += renderer.bounds.size.y / 2;
        TransformByAnchorSpace(objToTransform, newOffset, rotationOffset);
    }

    public static Vector3 TransformPointByAnchorSpace(Vector3 point)
    {
        GameObject anchor = fakeAnchor;

        if (ARController.getArStatus() == ARController.Status.Running)
        {
            // Has real AR anchor, so set object transform off of real anchor
            anchor = ARController.anchor.gameObject;
        }

        // Save anchor's and object's local scale
        Vector3 anchorScale = anchor.transform.localScale;

        // Set anchor to normal scale before transformation
        anchor.transform.localScale = Vector3.one;

        Vector3 transformedPoint = anchor.transform.TransformPoint(point);

        // Restore anchor's scale
        anchor.transform.localScale = anchorScale;

        return transformedPoint;
    }
}
