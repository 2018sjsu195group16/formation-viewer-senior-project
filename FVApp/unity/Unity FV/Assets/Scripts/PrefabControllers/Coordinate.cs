﻿using System.Collections.Generic;
using UnityEngine;

public class Coordinate : MonoBehaviour
{
    public enum ModelType { Person, CurrentTarget, NextTarget };

    // Coordinate variables
    private string coordinateId;
    private Vector3 anchorRelativePosition;
    private Vector3 orientation;
    private Vector3 modelRotation;
    private bool shouldAlignBottomToAnchor;
    private ModelType currentModelType;
    private Dictionary<ModelType, GameObject> models;
    private bool isInitialized = false;

    private Vector3 modelScaleFactorTopDown = new Vector3(4, 4, 4);
    private Vector3 modelScaleFactorAr = new Vector3(1, 1, 1);
    private Vector3 normalScale = Vector3.one;

    private MainController.ViewMode currentViewMode;

    // Model Prefabs
    // Rendered for coordinates determined as where "people" are standing
    public GameObject PersonPrefab;
    public Material PersonLightEstimatedMaterial;
    public Material PersonDefaultMaterial;

    /// <summary>
    /// The rotation in degrees need to apply to model when the CurrentTarget model is placed.
    /// </summary>
    private static readonly Vector3 currentTargetModelRotation = new Vector3(90, 0, 0);

    // Rendered for coordinates determined as where user should stand
    public GameObject CurrentTargetPrefab;
    public Material CurrentTargetLightEstimatedMaterial;
    public Material CurrentTargetDefaultMaterial;

    // Rendered for coordinates determined as where user's next location is
    public GameObject NextTargetPrefab;
    public Material NextTargetLightEstimatedMaterial;
    public Material NextTargetDefaultMaterial;

    public void Initialize(string id, ModelType modelType, Vector3 position, bool alignBottom)
    {
        orientation = Vector3.forward;
        Initialize(id, modelType, position, orientation, alignBottom);
    }

    public void Initialize(string id, ModelType modelType, Vector3 position, Vector3 direction, bool alignBottom)
    {
        coordinateId = id;
        anchorRelativePosition = position;
        orientation = direction;
        shouldAlignBottomToAnchor = alignBottom;

        // Create an object of each prefab to swtich between
        models = new Dictionary<ModelType, GameObject>();
        models.Add(ModelType.Person, Instantiate(PersonPrefab, transform.position, transform.rotation));
        models.Add(ModelType.CurrentTarget, Instantiate(CurrentTargetPrefab, transform.position, transform.rotation));
        models.Add(ModelType.NextTarget, Instantiate(NextTargetPrefab, transform.position, transform.rotation));

        foreach (KeyValuePair<ModelType, GameObject> entry in models)
        {
            // Make each model child of this game object
            entry.Value.transform.parent = transform;
            // Hide each child models
            entry.Value.SetActive(false);
        }

        ChangeModel(modelType);
        isInitialized = true;
    }

    public Vector3 GetPosition()
    {
        return anchorRelativePosition;
    }

    public string GetCoordinateId()
    {
        return coordinateId;
    }

    public bool GetIsInitialized()
    {
        return isInitialized;
    }

    public void SetViewMode(MainController.ViewMode viewMode)
    {
        //Debug.Log("transform models[currentModelType]: " + transform.localScale + " " + models[currentModelType].transform.localScale);
        //models[currentModelType].transform.localScale = normalScale;

        switch (viewMode)
        {
            case MainController.ViewMode.ArViewMode:
                // resize coordinates for AR View
                transform.localScale = modelScaleFactorAr;
                break;
            case MainController.ViewMode.TopDownViewMode:
                // resize coordinates for Top Down View
                transform.localScale = modelScaleFactorTopDown;
                break;
            default:
                throw new System.Exception("method ChangeViewMode: viewMode has an invalid value: " + viewMode);
        }

        currentViewMode = viewMode;
    }

    public void ChangeModel(ModelType modelType)
    {
        modelRotation = orientation;

        // Adjust custom rotation for current target
        if (modelType == ModelType.CurrentTarget)
        {
            modelRotation += currentTargetModelRotation;
        }

        // Hide current model type and show new one
        models[currentModelType].SetActive(false);
        models[modelType].SetActive(true);

        currentModelType = modelType;
        SetViewMode(currentViewMode);
    }

    // Update is called once per frame
    void Update()
    {
        // Update object to be at its position relative to the anchor
        if (shouldAlignBottomToAnchor)
        {
            MainController.TransformByAnchorSpaceHeightAdjusted(models[currentModelType].gameObject, anchorRelativePosition, modelRotation);
        }
        else
        {
            MainController.TransformByAnchorSpace(models[currentModelType].gameObject, anchorRelativePosition, modelRotation);
        }
    }

    // Removes light estimation from models
    public void enableLightEstimation(bool shouldEstimate)
    {
        GameObject prefab;
        Material mat;
        switch (currentModelType)
        {
            case ModelType.Person:
                prefab = PersonPrefab;
                if (shouldEstimate)
                {
                    mat = PersonLightEstimatedMaterial;
                }
                else
                {
                    mat = PersonDefaultMaterial;
                }
                break;
            case ModelType.CurrentTarget:
                prefab = CurrentTargetPrefab;
                if (shouldEstimate)
                {
                    mat = CurrentTargetLightEstimatedMaterial;
                }
                else
                {
                    mat = CurrentTargetDefaultMaterial;
                }
                break;
            case ModelType.NextTarget:
                prefab = NextTargetPrefab;
                if (shouldEstimate)
                {
                    mat = NextTargetLightEstimatedMaterial;
                }
                else
                {
                    mat = NextTargetDefaultMaterial;
                }
                break;
            default:
                throw new System.Exception("method enableLightEstimation: coordinate property models[currentModelType]Type has an invalid value: " + models[currentModelType]);
        }

        // Change material for prefabs
        PersonPrefab.GetComponentInChildren<Renderer>().material = PersonLightEstimatedMaterial;
        CurrentTargetPrefab.GetComponentInChildren<Renderer>().material = CurrentTargetLightEstimatedMaterial;
        NextTargetPrefab.GetComponentInChildren<Renderer>().material = NextTargetLightEstimatedMaterial;

        // Change material for self
        models[currentModelType].GetComponentInChildren<Renderer>().material = mat;
    }
}
