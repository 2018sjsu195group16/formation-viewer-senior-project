﻿//-----------------------------------------------------------------------
// <copyright file="FVAppARController.cs">
//
// Copyright Formation Viewr Team. All Rights Reserved.
//
// Not Licensed.
//
// </copyright>
//-----------------------------------------------------------------------

using UnityEngine;
using GoogleARCore;

#if UNITY_EDITOR
// Set up touch input propagation while using Instant Preview in the editor.
using Input = GoogleARCore.InstantPreviewInput;
#endif

/// <summary>
/// Controls FVApp. Modified from Google ARCore HelloAR example app.
/// </summary>
public class ARController : MonoBehaviour
{
    /// <summary>
    /// The first-person camera being used to render the passthrough camera image (i.e. AR background).
    /// </summary>
    public Camera FirstPersonCamera;

    /// <summary>
    /// A prefab for tracking and visualizing detected planes.
    /// </summary>
    public GameObject DetectedPlanePrefab;

    /// <summary>
    /// The model to place when a raycast from a user touch hits a plane.
    /// </summary>
    public GameObject StartPositionPrefab;

    public Material StartPositionLightEstimatedMaterial;
    public Material StartPositionDefaultMaterial;

    /// <summary>
    /// The rotation in degrees need to apply to model x when the start position (SP) model is placed.
    /// </summary>
    private const float k_ModelRotationX = 90;

    /// <summary>
    /// The rotation in degrees need to apply to model y when the start position (SP) model is placed.
    /// </summary>
    private const float k_ModelRotationY = 0;

    /// <summary>
    /// The rotation in degrees need to apply to model when the start position (SP) model is placed.
    /// </summary>
    private static readonly Vector3 k_ModelRotation = new Vector3(90, 0, 0);

    /// <summary>
    /// True if the app is in the process of quitting due to an ARCore connection error, otherwise false.
    /// </summary>
    private bool m_IsQuitting = false;

    /// <summary>
    /// True if the SP model has been placed.
    /// </summary>
    private static bool hasStartPosition = false;

    /// <summary>
    /// The anchor to serve as the formation environment's local origin.
    /// </summary>
    public static Anchor anchor;

    /// <summary>
    /// The direction that the SP model is facing.
    /// </summary>
    private static Vector3 spModelDirection;

    private static Vector3 spScaleFactorTopDown = new Vector3(4, 4, 4);
    private static Vector3 spScaleFactorAr = new Vector3(1, 1, 1);
    private static MainController.ViewMode currentViewMode;

    /// <summary>
    /// Possible Status constants
    /// </summary>
    public enum Status { Calibrating, Running };

    /// <summary>
    /// The Unity Update() method.
    /// </summary>
    public void Update()
    {
        _UpdateApplicationLifecycle();

        // If the player has not touched the screen, we are done with this update.
        // Also done if start position has already been placed
        // Also done if not in AR Mode
        Touch touch;
        if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began || hasStartPosition || currentViewMode == MainController.ViewMode.TopDownViewMode)
        {
            return;
        }

        // Raycast against the location the player touched to search for planes.
        TrackableHit hit;
        TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon;

        if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
        {
            // Use hit pose and camera pose to check if hit test is from the
            // back of the plane, if it is, no need to create the anchor.
            if ((hit.Trackable is DetectedPlane) &&
                Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                    hit.Pose.rotation * Vector3.up) < 0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else
            {
                // Instantiate SP model at the hit pose.
                var spObject = Instantiate(StartPositionPrefab, hit.Pose.position, hit.Pose.rotation);

                // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
                spObject.transform.Rotate(k_ModelRotation, Space.Self);

                // Get (x, 0, z) direction that SP model is facing;
                spModelDirection = hit.Pose.position - FirstPersonCamera.transform.position;
                spModelDirection.y = 0;

                // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
                // world evolves.
                anchor = hit.Trackable.CreateAnchor(hit.Pose);

                // Make SP model a child of the anchor.
                spObject.transform.parent = anchor.transform;

                anchor.GetComponentInChildren<Renderer>().material = StartPositionLightEstimatedMaterial;

                // Indicate start position has been placed
                hasStartPosition = true;
            }
        }
    }

    public void SetViewMode(MainController.ViewMode viewMode)
    {
        switch (viewMode)
        {
            case MainController.ViewMode.ArViewMode:
                if (getArStatus() == Status.Running)
                {
                    anchor.gameObject.transform.localScale = spScaleFactorAr;
                    anchor.GetComponentInChildren<Renderer>().material = StartPositionLightEstimatedMaterial;
                }
                break;
            case MainController.ViewMode.TopDownViewMode:
                if (getArStatus() == Status.Running)
                {
                    anchor.gameObject.transform.localScale = spScaleFactorTopDown;
                    anchor.GetComponentInChildren<Renderer>().material = StartPositionDefaultMaterial;
                }
                break;
            default:
                throw new System.Exception("method ChangeViewMode: viewMode has an invalid value: " + viewMode);
        }

        currentViewMode = viewMode;
    }

    /// <summary>
    /// Hides SP marker
    /// </summary>
    public static void SetSpActive(bool shouldShow)
    {
        if (anchor != null)
        {
            anchor.gameObject.SetActive(shouldShow);
        }
        else
        {
            Debug.Log("Anchor has not been created yet");
        }
    }

    /// <summary>
    /// Give current status
    /// </summary>
    public static Status getArStatus()
    {
        if (!hasStartPosition)
        {
            return Status.Calibrating;
        }
        else
        {
            return Status.Running;
        }
    }

    /// <summary>
    /// Check and update the application lifecycle.
    /// </summary>
    private void _UpdateApplicationLifecycle()
    {
        // Exit the app when the 'back' button is pressed.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        // Only allow the screen to sleep when not tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            const int lostTrackingSleepTimeout = 15;
            Screen.sleepTimeout = lostTrackingSleepTimeout;
        }
        else
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        if (m_IsQuitting)
        {
            return;
        }

        // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            _ShowAndroidToastMessage("Camera permission is needed to run this application.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
        else if (Session.Status.IsError())
        {
            _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
            m_IsQuitting = true;
            Invoke("_DoQuit", 0.5f);
        }
    }

    /// <summary>
    /// Actually quit the application.
    /// </summary>
    private void _DoQuit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Show an Android toast message.
    /// </summary>
    /// <param name="message">Message string to show in the toast.</param>
    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }
}
