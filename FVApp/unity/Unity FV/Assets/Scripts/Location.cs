﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Location : ScriptableObject
{
    public enum ReferencePoint { Center, FrontRight, FrontLeft, BackRight, BackLeft };

    // Location variables
    private string locationId;
    private string imagePath;
    private float lengthInMeters;
    private float widthInMeters;
    private Dictionary<ReferencePoint, Vector3> referencePoints;
    private bool isInitialized = false;

    // Create a new Location
    public void Initialize(string id, string pathToImage, float length, float width)
    {
        if (width < 0)
        {
            Debug.Log("method Initialize: width given is not positive: " + width);
            width = -width;
        }

        if (length < 0)
        {
            Debug.Log("method Initialize: length given is positive: " + length);
            length = -length;
        }

        locationId = id;
        imagePath = pathToImage;
        widthInMeters = width;
        lengthInMeters = length;
        CreateReferencePoints();
        isInitialized = true;
    }

    public string GetLocationId()
    {
        return locationId;
    }

    public float GetWidth()
    {
        return widthInMeters;
    }

    public float GetLength()
    {
        return lengthInMeters;
    }

    public bool GetIsInitialized()
    {
        return isInitialized;
    }

    private void CreateReferencePoints()
    {
        referencePoints = new Dictionary<ReferencePoint, Vector3>();

        // Add Center point
        referencePoints.Add(ReferencePoint.Center, Vector3.zero);

        // Add Front points
        referencePoints.Add(ReferencePoint.FrontRight, new Vector3(widthInMeters / 2F, 0, lengthInMeters / 2F));
        referencePoints.Add(ReferencePoint.FrontLeft, new Vector3(-widthInMeters / 2F, 0, lengthInMeters / 2F));

        // Add Back points
        referencePoints.Add(ReferencePoint.BackRight, new Vector3(widthInMeters / 2F, 0, -lengthInMeters / 2F));
        referencePoints.Add(ReferencePoint.BackLeft, new Vector3(-widthInMeters / 2F, 0, -lengthInMeters / 2F));
    }

    // Get the vector version of a location's reference points
    public Vector3 GetReferencePoint(ReferencePoint referencePointType)
    {
        if (referencePoints.ContainsKey(referencePointType))
        {
            return referencePoints[referencePointType];
        }
        else
        {
            throw new System.Exception("method GetReferencePoint: parameter referencePointType has an invalid value: " + referencePointType);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
