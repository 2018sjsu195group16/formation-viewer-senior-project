﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Application = UnityEngine.Application;
using BuildResult = UnityEditor.Build.Reporting.BuildResult;

public class Build : MonoBehaviour
{

    static readonly string ProjectPath = Path.GetFullPath(Path.Combine(Application.dataPath, ".."));
    static readonly string rootBuildsPath = Path.Combine(ProjectPath, "Builds");
    static readonly string apkPath = Path.Combine(rootBuildsPath, Application.productName + ".apk");

    [MenuItem("Build/Export Android %&a", false, 1)]
    public static void DoBuildAndroid()
    {
        string buildPath = Path.Combine(apkPath, Application.productName);
        string exportPath = Path.GetFullPath(Path.Combine(ProjectPath, "../../android/UnityExport"));

        if (Directory.Exists(apkPath))
            Directory.Delete(apkPath, true);

        if (Directory.Exists(exportPath))
            Directory.Delete(exportPath, true);

        if (Directory.Exists(rootBuildsPath))
            Directory.Delete(rootBuildsPath, true);

        Directory.CreateDirectory(rootBuildsPath);

        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;

        BuildOptions buildOptions = BuildOptions.AcceptExternalModificationsToPlayer;

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
        {
            scenes = GetEnabledScenes(),
            locationPathName = apkPath,
            target = BuildTarget.Android,
            options = buildOptions
        };

        UnityEngine.Debug.Log("Starting Android Build");

        UnityEditor.Build.Reporting.BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);

        if (report.summary.result != BuildResult.Succeeded)
            throw new Exception("Build failed");
   
        Copy(buildPath, exportPath);
        // Remove rootBuildsPath (recursively) not needed anymore
        if (Directory.Exists(rootBuildsPath))
            Directory.Delete(rootBuildsPath, true);

        // Modify build.gradle
		var build_file = Path.Combine(exportPath, "build.gradle");
		var build_text = File.ReadAllText(build_file);
        // Edit bracket indentation
        build_text = build_text.Replace("classpath 'com.android.tools.build:gradle:3.2.0'\n}", "classpath 'com.android.tools.build:gradle:3.2.0'\n    }");
        // Remove flat dir from repositories list
        build_text = build_text.Replace("        flatDir {\n            dirs 'libs'\n        }\n", "");
        // Make export into an android library instead of application
		build_text = build_text.Replace("com.android.application", "com.android.library");
        // Change jar search (for unity classes file) to reference created unity classes aar project
        build_text = build_text.Replace("implementation fileTree(dir: 'libs', include: ['*.jar'])", "api project(':unity-classes')");
        // Change aar file search to aar projects
        build_text = build_text.Replace("implementation(name: '", "implementation project(':");
        build_text = build_text.Replace("', ext:'aar')", "')");
        build_text = build_text.Replace("bundle {", "splits {");
        build_text = build_text.Replace("enableSplit = false", "enable false");
        build_text = build_text.Replace("enableSplit = true", "enable true");

        // build_text = build_text.Replace("implementation(name: 'VuforiaWrapper', ext:'aar')", "api(name: 'VuforiaWrapper', ext: 'aar')");
        build_text = Regex.Replace(build_text, @"\n.*applicationId '.+'.*", "");
		File.WriteAllText(build_file, build_text);

        // Modify AndroidManifest.xml
        var manifest_file = Path.Combine(exportPath, "src/main/AndroidManifest.xml");
        var manifest_text = File.ReadAllText(manifest_file);
        manifest_text = Regex.Replace(manifest_text, @"<application .*>", "<application>");
        Regex regex = new Regex(@"<activity.*>(\s|\S)+?</activity>", RegexOptions.Multiline);
        manifest_text = regex.Replace(manifest_text, "");
        File.WriteAllText(manifest_file, manifest_text);

        UnityEngine.Debug.Log("Finished Android Build");
    }

    static void Copy(string source, string destinationPath)
    {
        if (Directory.Exists(destinationPath))
            Directory.Delete(destinationPath, true);

        Directory.CreateDirectory(destinationPath);

        foreach (string dirPath in Directory.GetDirectories(source, "*",
            SearchOption.AllDirectories))
            Directory.CreateDirectory(dirPath.Replace(source, destinationPath));

        foreach (string newPath in Directory.GetFiles(source, "*.*",
            SearchOption.AllDirectories))
            File.Copy(newPath, newPath.Replace(source, destinationPath), true);
    }

    static string[] GetEnabledScenes()
    {
        var scenes = EditorBuildSettings.scenes
            .Where(s => s.enabled)
            .Select(s => s.path)
            .ToArray();

        return scenes;
    }
}
