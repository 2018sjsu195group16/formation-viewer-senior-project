﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopDownView : MonoBehaviour
{
    public enum LocationType { Football, Basketball };
    public Image footballImage;
    public Image basketballImage;
    private Image currentImage;

    // These specific x and y values interpret to zero in inspector for some reason. Z is only variable needed mathematically.
    private static readonly Vector3 footballImageRotationOffset = new Vector3(0, 0, -90);
    private static readonly Vector3 basketballImageRotationOffset = new Vector3(0, 0, 0);
    private static readonly Vector3 footballTopDownPositionOffset = new Vector3(0, 1.47F, 0);
    private Vector3 currentImageRotationOffset;

    private LocationType currentLocationType;
    private LocationType previousLocationType;

    private const float FOOTBALL_FIELD_LENGTH_YARDS = 120;
    private const float BASKETBALL_COURT_LENGTH_YARDS = 94 / 3;

    // Make sure camera is pointed at ground
    private Vector3 cameraRotationOffsetFromAnchor;
    private Dictionary<DeviceOrientation, Vector3> cameraRotationByOrientations;
    private DeviceOrientation currentDeviceOrientation;

    private Dictionary<LocationType, Location> locations;

    // Start is called before the first frame update
    void Start()
    {
        // Set initial variables
        currentImageRotationOffset = Vector3.zero;
        cameraRotationOffsetFromAnchor = new Vector3(90, 0, 90);
        currentDeviceOrientation = DeviceOrientation.Portrait;

        // Set up locations
        // TODO check widths/lengths given
        locations = new Dictionary<LocationType, Location>();

        Location footballField = ScriptableObject.CreateInstance<Location>();
        footballField.Initialize("football", "", ConvertYardsToMeters(FOOTBALL_FIELD_LENGTH_YARDS), ConvertYardsToMeters(53 + (1 / 3)));
        locations.Add(LocationType.Football, footballField);

        Location basketballCourt = ScriptableObject.CreateInstance<Location>();
        footballField.Initialize("basketball", "", ConvertYardsToMeters(BASKETBALL_COURT_LENGTH_YARDS), ConvertYardsToMeters(50 / 3));
        locations.Add(LocationType.Basketball, basketballCourt);

        // Set up device orientaion camera rotations
        cameraRotationByOrientations = new Dictionary<DeviceOrientation, Vector3>();
        cameraRotationByOrientations.Add(DeviceOrientation.LandscapeLeft, new Vector3(0, 0, -90));
        cameraRotationByOrientations.Add(DeviceOrientation.LandscapeRight, new Vector3(0, 0, -90));
        cameraRotationByOrientations.Add(DeviceOrientation.Portrait, new Vector3(0, 0, 0));
        cameraRotationByOrientations.Add(DeviceOrientation.PortraitUpsideDown, new Vector3(0, 0, 180));
    }

    public static float ConvertYardsToMeters(float yards)
    {
        return yards / 1.094F;
    }

    private void HideImages()
    {
        footballImage.gameObject.SetActive(false);
        basketballImage.gameObject.SetActive(false);
    }

    public void SetViewMode(MainController.ViewMode viewMode)
    {
        switch (viewMode)
        {
            case MainController.ViewMode.ArViewMode:
                footballImage.rectTransform.anchoredPosition3D = Vector3.zero;
                break;
            case MainController.ViewMode.TopDownViewMode:
                // Change Football Image position to align better
                footballImage.rectTransform.anchoredPosition3D = footballTopDownPositionOffset;
                break;
            default:
                throw new System.Exception("method ChangeViewMode: viewMode has an invalid value: " + viewMode);
        }
    }

    public Vector3 GetReferencePoint(Location.ReferencePoint referencePoint)
    {
        return locations[currentLocationType].GetReferencePoint(referencePoint);
    }

    public void SetDeviceOrientation(DeviceOrientation orientation)
    {
        if (orientation == DeviceOrientation.FaceUp || orientation == DeviceOrientation.FaceDown || orientation == DeviceOrientation.Unknown)
        {
            Debug.Log("method SetDeviceOrientation: Ignoring unsupported orientation given: " + orientation);
            return;
        }

        currentDeviceOrientation = orientation;
    }

    public void SetLocationType(LocationType locationType)
    {
        currentLocationType = locationType;

        float locationLength;

        switch (currentLocationType)
        {
            case LocationType.Football:
                locationLength = ConvertYardsToMeters(FOOTBALL_FIELD_LENGTH_YARDS);
                currentImage = footballImage;
                currentImageRotationOffset = footballImageRotationOffset;
                break;
            case LocationType.Basketball:
                locationLength = ConvertYardsToMeters(BASKETBALL_COURT_LENGTH_YARDS);
                currentImage = basketballImage;
                currentImageRotationOffset = basketballImageRotationOffset;
                break;
            default:
                throw new System.Exception("method SetLocationType: currentLocationType has an invalid value: " + currentLocationType);
        }

        // Camera orhtographic size should be set to 0.5 * desired height (in meters)
        GetComponent<Camera>().orthographicSize = locationLength * 0.5F;

        // Hide all images (only one can be shown at a time)
        HideImages();

        // Show image for location type
        currentImage.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        // If no current image yet, done with update
        if (currentImage == null)
        {
            return;
        }

        if (currentLocationType != previousLocationType)
        {
            SetLocationType(currentLocationType);
            previousLocationType = currentLocationType;
        }

        // Adjust position of top down view but keep camera height constant
        float cameraHeight = transform.position.y;

        // Adjust Location images by negative of current device orientation
        currentImage.rectTransform.eulerAngles = currentImageRotationOffset + transform.eulerAngles - cameraRotationByOrientations[currentDeviceOrientation];

        // Adjust Camera in relation to anchor
        MainController.TransformByAnchorSpace(gameObject, Vector3.zero, cameraRotationOffsetFromAnchor + cameraRotationByOrientations[currentDeviceOrientation]);

        // Adjust Camera rotation by current device orientation 
        Vector3 cameraPosition = transform.position;
        cameraPosition.y = cameraHeight;
        transform.position = cameraPosition;
    }
}
