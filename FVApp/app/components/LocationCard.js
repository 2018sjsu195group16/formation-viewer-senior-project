import React, { Component } from 'react'
import { StyleSheet, Image } from 'react-native'
import PropTypes from 'prop-types'
import {
  Text,
  Card,
  Body,
  CardItem,
} from 'native-base'

export default class LocationCard extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    locationTitle: PropTypes.string.isRequired,
    locationId: PropTypes.string.isRequired,
    //locationImageRef: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired,
  }

  handlePickedLocation = () => {
    this.props.actions.selectLocation({
      id: this.props.locationId,
    })
    this.props.navigation.navigate('Views')
  }

  render() {
    return (
      <Card style={styles.card}>
        <CardItem bordered button onPress={this.handlePickedLocation}>
          <Text style={styles.cardTitleText}>Location:</Text>
          <Text> {this.props.locationTitle} </Text>
        </CardItem>
        <CardItem style={styles.cardBody} button onPress={this.handlePickedLocation}>
          <Body style={styles.cardBody}>
            <Image source={this.props.locationImageRef} style={{ width: 300, height: 180 }} />
          </Body>
        </CardItem>
      </Card>
    )
  }
}

const styles = StyleSheet.create({
  card: {
    marginTop: 10,
  },
  cardTitleText: {
    fontWeight: 'bold'
  },
  cardBody: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
})