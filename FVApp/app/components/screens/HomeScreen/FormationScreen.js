import React, { Component } from 'react'
import { StyleSheet, StatusBar, Platform } from 'react-native'
import Icons from 'react-native-vector-icons/Ionicons'
import { DocumentPicker } from 'react-native-document-picker'
import * as RNFS from 'react-native-fs'
import PropTypes from 'prop-types'
import FormationCardContainer from '../../../containers/FormationCardContainer'
import {
  Card,
  CardItem,
  CardBody,
  Icon,
  Button,
  Content,
  Container,
} from 'native-base'

export default class FormationScreen extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    formationLists: PropTypes.array.isRequired
  }

  pickFile = () => {
    // Modified from https://github.com/Elyx0/react-native-document-picker
    // iPhone/Android
    DocumentPicker.show({
      filetype: ['application/*'], // for android, is mime type may be different for iOS
    }, (error, res) => {
      if (error) {
        console.log('error getting file: ', error)
        return
      }
      // Android
      /*
      console.log(
        'uri: ', res.uri,
        '\nmime type: ', res.type, // mime type
        '\nfile name: ', res.fileName,
        '\nfile size: ', res.fileSize
      )
      */
      if (!res.fileName.endsWith('.json')) {
        console.log('is not a json extension')
        return
      }

      RNFS.readFile(res.uri).then((contents) => {
        this.props.actions.addFormationList({
          formationList: JSON.parse(contents),
        })
      })
        .catch((err) => {
          console.error(err.message, err.code)
        })
    })

    /* iPad
    const { pageX, pageY } = event.nativeEvent

    DocumentPicker.show({
        top: pageY,
        left: pageX,
        filetype: ['public.image'],
    }, (error, url) => {
        alert(url)
    })
    */
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor={styles.toolbar.backgroundColor} />
        <Icons.ToolbarAndroid
          contentInsetStart={100}
          style={styles.toolbar}
          navIconName='md-menu'
          title='Formation Viewer'
          titleColor='white'
          onIconClicked={() => this.props.navigation.toggleDrawer()}
        />
        <Content padder contentContainerStyle={styles.content}>
          {this.props.formationLists.map((formationList) => {
            return (
              <FormationCardContainer
                key={formationList.id}
                formationListTitle={formationList.title}
                formationListId={'' + formationList.id}
                navigation={this.props.navigation}
              />
            )
          })}
          <Button transparent large info onPress={this.pickFile} style={{ width: 70, height: 70, alignSelf: 'center' }}>
            <Icon name='add-circle' style={{ fontSize: 40, alignSelf: 'center' }} />
          </Button>

        </Content>
      </Container>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  content: {
    flex: 1,
  },
  toolbar: {
    height: 46,
    backgroundColor: 'blue',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
})