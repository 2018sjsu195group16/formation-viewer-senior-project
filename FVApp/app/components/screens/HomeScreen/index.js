import React from 'react'
import { Icon } from 'native-base'
import FormationScreenContainer from '../../../containers/FormationScreenContainer'
import LocationScreenContainer from '../../../containers/LocationScreenContainer'
import ViewsScreenContainer from '../../../containers/ViewScreenContainer'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'

const styles = {
  formation: {
    tabBarColor: 'blue',
  },
  location: {
    tabBarColor: 'orange',
  },
  views: {
    tabBarColor: '#23ba4b',
  },
}

export default createMaterialBottomTabNavigator(
  {
    Formation: {
      screen: FormationScreenContainer,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Icon name='paper' style={{ color: 'white' }} />
          }
          else {
            return <Icon name='paper' style={{ color: styles.formation.tabBarColor }} />
          }
        },
        tabBarColor: styles.formation.tabBarColor,
      }
    },
    Location: {
      screen: LocationScreenContainer,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Icon name='pin' style={{ color: 'white' }} />
          }
          else {
            return <Icon name='pin' style={{ color: styles.location.tabBarColor }} />
          }
        },
        tabBarColor: styles.location.tabBarColor
      }
    },
    Views: {
      screen: ViewsScreenContainer,
      navigationOptions: {
        tabBarIcon: ({ focused }) => {
          if (focused) {
            return <Icon name='eye' style={{ color: 'white' }} />
          }
          else {
            return <Icon name='eye' style={{ color: styles.views.tabBarColor }} />
          }
        },
        tabBarColor: styles.views.tabBarColor
      }
    },
  },
  {
    shifting: true,
    activeColor: 'white',
    barStyle: { color: '#3e2465' }
  }
)


