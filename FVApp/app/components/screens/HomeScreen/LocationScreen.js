import React, { Component } from 'react'
import { StyleSheet, Image, StatusBar, Platform, Picker } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import LocationCardContainer from '../../../containers/LocationCardContainer'
import PropTypes from 'prop-types'
import {
  Content,
  Container,
} from 'native-base'

const images = [
  require('../../../assets/us-football-field-bw.png'),
  require('../../../assets/basketball-court-bw-horizontal.png'),
]

export default class LocationScreen extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    locations: PropTypes.array.isRequired
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor={styles.toolbar.backgroundColor} />
        <Icon.ToolbarAndroid
          style={styles.toolbar}
          navIconName='md-menu'
          title='Formation Viewer'
          titleColor='white'
          onIconClicked={() => this.props.navigation.toggleDrawer()}
        />
        <Content padder >
          {this.props.locations.map((location, index) => {
            return (
              <LocationCardContainer
                key={location.id}
                locationTitle={location.title}
                locationId={location.id}
                locationImageRef={images[index]}
                navigation={this.props.navigation}
              />
            )
          })}
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 30,
    alignItems: 'center'
  },
  toolbar: {
    height: 46,
    backgroundColor: 'orange',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  title: {
    alignItems: 'center',
    flexDirection: 'row',
    width: 80,
  }
})