import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, Platform, Picker } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import PropTypes from 'prop-types'
import Modal from 'react-native-modal'
import ExtraDimensions from 'react-native-extra-dimensions-android'
import {
  Text,
  Button,
  Container,
  Card,
  CardItem,
} from 'native-base'
import UnityView, {
  UnityModule
} from 'react-native-unity-view'

export default class ViewScreen extends Component {
  disclaimerText = 'Please be careful and be aware of your surroundings.\n\nReal-world objects may sometimes be hidden by virtual ones.'

  toolbarActions = [
    {
      title: 'AR View', iconName: 'md-glasses', iconSize: 30, show: 'always',
      actionFunction: () => {
        if (!this.state.disclaimerRead) {
          alert(this.disclaimerText)
          this.setState({ disclaimerRead: true })
        }

        UnityModule.postMessageToUnityManager({
          name: 'set view mode',
          data: {
            viewMode: !this.state.showArView ? 0 : 1,
          },
        })

        this.setState({ showArView: !this.state.showArView })
      },
    },
    {
      title: 'Next Formation', show: 'never',
      actionFunction: () => {
        // TODO handle error better (prevent going to next/previous if at either end)
        // if index is not valid, print error and return
        if (this.state.formationIndex + 1 < 0 || this.state.formationIndex + 1 >= this.props.formationList.formations.length) {
          console.log('Cannot go to next formation. Index is invalid: ', this.state.formationIndex + 1)
          return
        }

        UnityModule.postMessageToUnityManager({
          name: 'set formation',
          data: {
            formationIndex: this.state.formationIndex + 1,
          },
        })

        this.setState({ formationIndex: this.state.formationIndex + 1 })
      },
    },
    {
      title: 'Previous Formation', show: 'never',
      actionFunction: () => {
        // TODO handle error better (prevent going to next/previous if at either end)
        // if index is not valid, print error and return
        if (this.state.formationIndex - 1 < 0 || this.state.formationIndex - 1 >= this.props.formationList.formations.length) {
          console.log('Cannot go to previous formation. Index is invalid: ', this.state.formationIndex - 1)
          return
        }

        UnityModule.postMessageToUnityManager({
          name: 'set formation',
          data: {
            formationIndex: this.state.formationIndex - 1,
          },
        })

        this.setState({ formationIndex: this.state.formationIndex - 1 })
      },
    },
    {
      title: 'Toggle 3D Models', show: 'never',
      actionFunction: () => {
        UnityModule.postMessageToUnityManager({
          name: 'set show models',
          data: {
            shouldShowModels: !this.state.shouldShowModels,
          },
        })

        this.setState({ shouldShowModels: !this.state.shouldShowModels })
      },
    },
    {
      title: 'Toggle SP Marker', show: 'never',
      actionFunction: () => {
        UnityModule.postMessageToUnityManager({
          name: 'set show SP marker',
          data: {
            shouldShowSpMarker: !this.state.shouldShowSpMarker,
          },
        })

        this.setState({ shouldShowSpMarker: !this.state.shouldShowSpMarker })
      },
    },
    {
      title: 'Select Coordinate ID', show: 'never',
      actionFunction: () => {
        this.setState({ shouldShowCoordinateIdModal: true })
      },
    },
    {
      title: 'Select Reference Point', show: 'never',
      actionFunction: () => {
        this.setState({ shouldShowReferencePointModal: true })
      },
    },
    {
      title: 'Toggle Next Position', show: 'never',
      actionFunction: () => {
        UnityModule.postMessageToUnityManager({
          name: 'set show next position',
          data: {
            shouldShowNextPosition: !this.state.shouldShowNextPosition,
          },
        })

        this.setState({ shouldShowNextPosition: !this.state.shouldShowNextPosition })
      },
    },
    {
      title: 'View Pathing', show: 'never',
      actionFunction: () => {
        this.setState({ shouldShowPathingModal: true })
      },
    },
    {
      title: 'Change Location', show: 'never',
      actionFunction: () => {
        UnityModule.postMessageToUnityManager({
          name: 'set location',
          data: {
            locationId: this.props.location,
          },
        })

        this.setState({ shouldShowNextPosition: !this.state.shouldShowNextPosition })
      },
    },
  ]

  static propTypes = {
    actions: PropTypes.object.isRequired,
    formationList: PropTypes.object.isRequired,
    coordinateId: PropTypes.string,
    validCoordinateIds: PropTypes.array.isRequired,
  }

  state = {
    renderToolbar: true,
    renderUnity: false,
    showArView: false,
    disclaimerRead: false,
    calibrationComplete: false,
    formationIndex: 0,
    referencePointType: 0,
    selectedCoordinateId: this.props.coordinateId,
    pathingText: "Please select a Coordinate ID to see it's pathing information.",
    shouldShowModels: true,
    shouldShowSpMarker: false,
    shouldShowNextPosition: true,
    shouldShowCoordinateIdModal: false,
    shouldShowReferencePointModal: false,
    shouldShowPathingModal: false,
  }

  referencePointTypes = [
    { label: '', value: -1 },
    { label: 'Center', value: 0 },
    { label: 'Front Right Corner', value: 1 },
    { label: 'Front Left Corner', value: 2 },
    { label: 'Back Right Corner', value: 3 },
    { label: 'Back Left Corner', value: 4 },
  ]

  handleToolbarAction = (position) => {
    this.toolbarActions[position].actionFunction()
  }

  handleUnityMessage = (message) => {
    switch (message.name) {
      case 'request formation list':
        setTimeout(() => {
          message.send({
            coordinateId: this.props.coordinateId,
            jsonString: JSON.stringify(this.props.formationList),
          })
        }, 2000)
        break
      case 'request location':
        setTimeout(() => {
          message.send({
            locationId: this.props.location,
          })
        }, 2000)
        break
      case 'calibration complete':
        this.setState({ calibrationComplete: true })
        break
      default:
        console.log('invalid message from Unity:\n', message)
        break
    }
  }

  handleUnityTouch = () => {
    if (!this.state.showArView || this.state.calibrationComplete || !this.state.renderToolbar) {
      this.setState({ renderToolbar: !this.state.renderToolbar })
    } else {
      return false
    }
  }

  handleCancelPathingModal = () => {
    this.setState({ shouldShowPathingModal: false })
  }

  handleCancelReferencePointModal = () => {
    this.setState({ referencePointType: 0 })
    this.setState({ shouldShowReferencePointModal: false })
  }

  handleSubmitReferencePointType = () => {
    if (this.state.referencePointType < -1 || this.state.referencePointType > 4) {
      return
    }

    UnityModule.postMessageToUnityManager({
      name: 'set reference point type',
      data: {
        referencePointType: this.state.referencePointType,
      },
    })

    console.log('sent: ', {
      referencePointType: this.state.referencePointType,
    })

    this.setState({ shouldShowReferencePointModal: false })
  }

  handleCancelCoordinateIdModal = () => {
    this.props.actions.selectCoordinateId('')
    this.setState({ shouldShowCoordinateIdModal: false })
  }

  handleSubmitCoordinateId = () => {
    if (this.props.coordinateId !== '') {
      UnityModule.postMessageToUnityManager({
        name: 'set coordinate ID',
        data: {
          coordinateId: this.state.selectedCoordinateId,
        },
      })

      this.setState({ shouldShowCoordinateIdModal: false })
      this.referencePointTypes[0].label = this.state.selectedCoordinateId
    }

    // get pathing text
    currentCoordinates = this.props.formationList.formations[this.state.formationIndex].coordinates

    indexWanted = currentCoordinates.findIndex((item) => {
      return item.id.toString() === this.state.selectedCoordinateId.toString()
    })

    if (indexWanted === -1) {
      this.setState({ pathingText: "Please select a Coordinate ID to see it's pathing information." })
    } else {
      path = currentCoordinates[indexWanted].path
      this.setState({ pathingText: 'Path Type: ' + path.type + '\nPath Details: ' + path.details })
    }
  }

  render() {
    let unityElement = (
      <UnityView
        onUnityMessage={this.handleUnityMessage}
        style={styles.unity}
      />
    )

    const deviceWidth = Platform.OS === 'ios' ? Dimensions.get('window').width : ExtraDimensions.get('REAL_WINDOW_WIDTH')
    const deviceHeight = Platform.OS === 'ios' ? Dimensions.get('window').height : ExtraDimensions.get('REAL_WINDOW_HEIGHT')

    const filteredReferencePointTypes = this.referencePointTypes.filter(item => {
      if (item.label === '') {
        return false
      }
      return true
    })

    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor={styles.toolbar.backgroundColor} />
        {this.state.renderToolbar && (
          <Icon.ToolbarAndroid
            style={styles.toolbar}
            navIconName='md-menu'
            title='Formation Viewer'
            titleColor='white'
            actions={this.toolbarActions}
            overflowIconName='md-more'
            onActionSelected={this.handleToolbarAction}
            onIconClicked={() => this.props.navigation.toggleDrawer()}
          />
        )}
        <Modal
          isVisible={this.state.shouldShowReferencePointModal}
          onBackdropPress={this.handleCancelReferencePointModal}

          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
        >
          <Card style={styles.card}>
            <CardItem bordered button onPress={this.handlePickedLocation}>
              <Picker
                style={{ height: 50, width: deviceWidth * 0.7 }}
                selectedValue={this.state.referencePointType}
                onValueChange={(itemValue) =>
                  this.setState({ referencePointType: itemValue })
                }>
                {filteredReferencePointTypes.map((item => <Picker.Item key={item.label + item.value} label={item.label} value={item.value} />))}
              </Picker>
            </CardItem>
            <CardItem style={styles.cardBody} button onPress={this.handlePickedLocation}>
              <Button small primary onPress={this.handleCancelReferencePointModal}>
                <Text> CANCEL </Text>
              </Button>
              <Button small primary onPress={this.handleSubmitReferencePointType}>
                <Text> OK </Text>
              </Button>
            </CardItem>
          </Card>
        </Modal>
        <Modal
          isVisible={this.state.shouldShowCoordinateIdModal}
          onBackdropPress={this.handleCancelCoordinateIdModal}
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
        >
          <Card style={styles.card}>
            <CardItem bordered>
              <Picker
                style={{ height: 50, width: deviceWidth * 0.7 }}
                selectedValue={this.state.selectedCoordinateId}
                onValueChange={(itemValue) => {
                  this.props.actions.selectCoordinateId(itemValue)
                  this.setState({ selectedCoordinateId: itemValue })
                }
                }>
                <Picker.Item label='-N/A-' value={''} />
                {this.props.validCoordinateIds.map((coordinateId) => {
                  return (
                    <Picker.Item key={coordinateId} label={coordinateId} value={coordinateId} />
                  )
                })}
              </Picker>
            </CardItem>
            <CardItem style={styles.cardBody}>
              <Button small primary onPress={this.handleCancelCoordinateIdModal}>
                <Text> CANCEL </Text>
              </Button>
              <Button small primary onPress={this.handleSubmitCoordinateId}>
                <Text> OK </Text>
              </Button>
            </CardItem>
          </Card>
        </Modal>
        <Modal
          isVisible={this.state.shouldShowPathingModal}
          onBackdropPress={this.handleCancelPathingModal}
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
        >
          <Card style={styles.card}>
            <CardItem bordered>
              <Text style={styles.cardTitleText}>Pathing Text</Text>
            </CardItem>
            <CardItem style={styles.textCardBody}>
              <Text>{this.state.pathingText}</Text>
            </CardItem>
            <CardItem style={styles.textCardBody}>
              <Button small primary onPress={this.handleCancelPathingModal}>
                <Text> OK </Text>
              </Button>
            </CardItem>
          </Card>
        </Modal>
        <View onStartShouldSetResponder={this.handleUnityTouch} style={styles.unityContainer}>
          {unityElement}
        </View>
      </Container >
    )
  }
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 30,
    alignItems: 'center'
  },
  title: {
    alignItems: 'center',
    flexDirection: 'row',
    width: 80
  },
  toolbar: {
    height: 46,
    backgroundColor: '#23ba4b',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  unityContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  unity: {
    position: 'absolute', left: 0, right: 0, top: 0, bottom: 0
  },
  viewModal: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    //justifyContent: 'space-between',
  },
  card: {
    backgroundColor: 'white',
  },
  cardTitleText: {
    fontWeight: 'bold'
  },
  cardBody: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textCardBody: {
    alignItems: 'center',
    justifyContent: 'center',
  }
})