import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/Ionicons'
import { StyleSheet, Image, Platform, StatusBar } from 'react-native'
import {
  Content,
  Text,
  Container,
} from 'native-base'

const icons = {
  FVicon: require('../../assets/fv-logo.png'),
}

export default class AboutScreen extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor={styles.toolbar.backgroundColor} />
        <Icon.ToolbarAndroid
          style={styles.toolbar}
          navIconName='md-menu'
          title='Formation Viewer'
          titleColor='white'
          onIconClicked={() => this.props.navigation.toggleDrawer()}
        />
        <Content padder contentContainerStyle={styles.content}>
          <Image source={icons.FVicon} style={{ width: 120, height: 120 }} />
          <Text style={styles.text}>Formation Viewer</Text>
          <Text style={{ color: '#38b2ff' }}>Version: 1.00</Text>
          <Text style={{ fontSize: 22 }}> Developed By: </Text>
          <Text style={styles.FVteam}> Matt Morin </Text>
          <Text style={styles.FVteam}> Sanjana Shetty </Text>
          <Text style={styles.FVteam}> Lianshi Gan </Text>
          <Text style={styles.FVteam}> Samuel Lau </Text>
        </Content>
      </Container>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 30,
    alignItems: 'center'
  },
  FVteam: {
    fontSize: 18,
    alignItems: 'center',
    fontFamily: 'serif'
  },
  toolbar: {
    height: 46,
    backgroundColor: 'black',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
})