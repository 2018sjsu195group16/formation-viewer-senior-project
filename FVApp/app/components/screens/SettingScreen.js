import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/Ionicons'
import { StyleSheet } from 'react-native'
import {
  Content,
  Text,
  Container,
} from 'native-base'

export default class SettingScreen extends Component {
  render() {
    return (
      <Container style={styles.container}>
        <Icon.ToolbarAndroid
          style={styles.toolbar}
          navIconName='md-menu'
          title='Formation Viewer'
          titleColor='white'
          onIconClicked={() => this.props.navigation.toggleDrawer()}
        />
        <Content padder contentContainerStyle={styles.content}>
          <Text style={styles.text}>Setting Page</Text>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 30,
    alignItems: 'center'
  },
  header: {
    backgroundColor: '#092756',
    alignItems: 'center',
  },
  headerText: {
    color: '#F5FCFF',
    fontSize: 18,
    fontFamily: 'serif',
    marginLeft: 5
  },
  title: {
    alignItems: 'center',
    flexDirection: 'row',
    width: 80

  }
})