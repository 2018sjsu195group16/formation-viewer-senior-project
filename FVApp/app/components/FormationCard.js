import React, { Component } from 'react'
import { StyleSheet, TextInput, Platform, Alert } from 'react-native'
import Modal from 'react-native-modal'
import PropTypes from 'prop-types'
import ExtraDimensions from 'react-native-extra-dimensions-android'
import {
  Button,
  Text,
  Card,
  CardItem,
  View,
} from 'native-base'

export default class FormationCard extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    formationListTitle: PropTypes.string.isRequired,
    formationListId: PropTypes.string.isRequired,
    navigation: PropTypes.object.isRequired,
  }

  state = {
    shouldShowRenameModal: false,
    formationListTitle: this.props.formationListTitle,
  }

  handlePickedFormationList = () => {
    this.props.actions.selectFormationList({
      id: this.props.formationListId,
    })
    this.props.navigation.navigate('Location')
  }

  handleSubmitRename = () => {
    if (this.state.formationListTitle === '') {
      return
    }

    this.props.actions.renameFormationList({
      title: this.state.formationListTitle,
      id: this.props.formationListId,
    })

    this.setState({ shouldShowRenameModal: false })
  }

  handleCancelRenameModal = () => {
    this.setState({
      formationListTitle: this.props.formationListTitle,
      shouldShowRenameModal: false
    })
  }

  handleDelete = () => {
    Alert.alert(
      'Delete Formation',
      "Delete '" + this.props.formationListTitle + "'?",
      [
        {
          text: 'CANCEL',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK', onPress: () => this.props.actions.deleteFormationList({
            id: this.props.formationListId,
          })
        },
      ],
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <Card style={styles.card}>
          <CardItem bordered button onPress={this.handlePickedFormationList}>
            <Text style={styles.cardTitleText}>Title:</Text>
            <Text> {this.props.formationListTitle} </Text>
          </CardItem>
          <CardItem style={styles.cardBody}>
            <Button small primary onPress={() => this.setState({ shouldShowRenameModal: true })}>
              <Text> RENAME </Text>
            </Button>
            <Button small primary onPress={this.handleDelete}>
              <Text> DELETE </Text>
            </Button>
          </CardItem>
        </Card>
        <Modal
          isVisible={this.state.shouldShowRenameModal}
          onBackdropPress={this.handleCancelRenameModal}
        >
          <Card style={styles.card}>
            <CardItem bordered>
              <Text style={styles.cardTitleText}>New Title:</Text>
              <TextInput
                autoFocus={true}
                value={this.state.formationListTitle}
                onChangeText={newTitle => this.setState({ formationListTitle: newTitle })}
                selectTextOnFocus={true}
              />
            </CardItem>
            <CardItem style={styles.cardBody}>
              <Button small primary onPress={this.handleCancelRenameModal}>
                <Text> CANCEL </Text>
              </Button>
              <Button small primary onPress={this.handleSubmitRename}>
                <Text> OK </Text>
              </Button>
            </CardItem>
          </Card>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  card: {
    marginTop: 10,
  },
  cardTitleText: {
    fontWeight: 'bold'
  },
  cardBody: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  container: {
  },
})