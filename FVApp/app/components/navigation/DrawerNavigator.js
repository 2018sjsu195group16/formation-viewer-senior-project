import React from 'react'
import { Platform, Dimensions } from 'react-native'
import { createDrawerNavigator, createAppContainer } from 'react-navigation'
import { Icon } from 'native-base'

import HomeScreen from '../screens/HomeScreen'
import AboutScreen from '../screens/AboutScreen'
//import SettingScreen from '../screens/SettingScreen'

const WIDTH = Dimensions.get('window').width

const DrawerConfig = {
  drawerWidth: WIDTH * 0.5,
}

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        drawerIcon: ({ focused }) => {
          if (focused) {
            return <Icon name='home' style={{ color: '#38b2ff' }} />
          }
          else {
            return <Icon name='home' style={{ color: '#092756' }} />
          }
        }
      }
    },
    /*Setting: { 
        screen: SettingScreen,
        navigationOptions:{
            drawerIcon: ({focused}) => {
                if(focused){
                    return <Icon name= 'settings' style={{ color:'#38b2ff' }} />
                }
                else{
                    return <Icon name= 'settings' style={{color:'#092756'}} />
                }
            }           
        }  
    },*/
    About: {
      screen: AboutScreen,
      navigationOptions: {
        drawerIcon: ({ focused }) => {
          if (focused) {
            return <Icon name='information-circle' style={{ color: '#38b2ff' }} />
          }
          else {
            return <Icon name='information-circle' style={{ color: '#092756' }} />
          }
        }
      }
    },
  },
  DrawerConfig
)

export default createAppContainer(DrawerNavigator)