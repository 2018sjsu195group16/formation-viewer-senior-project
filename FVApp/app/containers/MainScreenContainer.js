import { connect } from "react-redux";
import MainScreen from "../components/MainScreen";
import { setWelcomeText, setSnowmmanColor } from "../ducks/appSettings";
import { bindActionCreators } from "redux";

const mapStateToProps = state => {
  return {
      welcomeText: state.appSettings.welcomeText
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators({ setWelcomeText }, dispatch)
});

const MainScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainScreen);

export default MainScreenContainer;
