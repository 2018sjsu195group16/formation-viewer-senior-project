import { connect } from 'react-redux'
import FormationScreen from '../components/screens/HomeScreen/FormationScreen'
import { addFormationList } from '../ducks/formation'
import { bindActionCreators } from 'redux'

const mapStateToProps = state => {
  return {
    formationLists: state.formation.formationLists,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators({addFormationList }, dispatch),
})

const FormationScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FormationScreen)

export default FormationScreenContainer
