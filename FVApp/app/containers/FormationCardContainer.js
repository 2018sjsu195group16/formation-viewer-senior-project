import { connect } from 'react-redux'
import FormationCard from '../components/FormationCard'
import { selectFormationList, renameFormationList, deleteFormationList } from '../ducks/formation'
import { bindActionCreators } from 'redux'

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators({ selectFormationList, renameFormationList, deleteFormationList }, dispatch),
})

const FormationCardContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FormationCard)

export default FormationCardContainer
