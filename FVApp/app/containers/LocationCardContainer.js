import { connect } from 'react-redux'
import LocationCard from '../components/LocationCard'
import { selectLocation } from '../ducks/formation'
import { bindActionCreators } from 'redux'

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators({ selectLocation }, dispatch),
})

const LocationCardContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationCard)

export default LocationCardContainer
