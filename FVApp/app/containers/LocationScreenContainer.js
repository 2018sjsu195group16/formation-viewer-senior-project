import { connect } from 'react-redux'
import LocationScreen from '../components/screens/HomeScreen/LocationScreen'
import { selectLocation } from '../ducks/formation'
import { bindActionCreators } from 'redux'

const mapStateToProps = state => {
  return {
    locations: state.formation.locations
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators({ selectLocation }, dispatch)
})

const LocationScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationScreen)

export default LocationScreenContainer
