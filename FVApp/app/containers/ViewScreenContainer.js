import { connect } from 'react-redux'
import ViewsScreen from '../components/screens/HomeScreen/ViewsScreen'
import { selectCoordinateId } from '../ducks/formation'
import { bindActionCreators } from 'redux'

const mapStateToProps = state => {
  validCoordinateIds = []
  var formationListJson

  indexWanted = state.formation.formationLists.findIndex((item) => {
    return item.id.toString() === state.formation.selectedFormationListId.toString()
  })

  if (indexWanted !== -1) {
    formationListJson = state.formation.formationLists[indexWanted]

    for (coordinate of formationListJson.formations[0].coordinates) {
      validCoordinateIds.push(coordinate.id)
    }
  }

  return {
    coordinateId: state.formation.selectedCoordinateId,
    formationList: formationListJson,
    location: state.formation.selectedLocationId,
    validCoordinateIds: validCoordinateIds,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators({ selectCoordinateId }, dispatch)
})

const ViewScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewsScreen)

export default ViewScreenContainer
