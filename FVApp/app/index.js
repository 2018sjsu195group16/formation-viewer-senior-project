<script src='http://localhost:8097' /> // react devtools connection

import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { Text, AppState } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import configureAppStore from './ducks'
import DrawerNavigator from './components/navigation/DrawerNavigator'

const store = configureAppStore()

// From https://medium.com/@sumitkushwaha/syncing-redux-store-with-asyncstorage-in-react-native-2b8b890b9ca1
class App extends Component {
  state = {
    isStoreLoading: false,
    store: store,
  }

  componentWillMount() {
    AppState.addEventListener('change', this._handleAppStateChange.bind(this))
    this.setState({ isStoreLoading: true })
    AsyncStorage.getItem('completeStore').then((completeStore) => {
      if (completeStore && completeStore.length) {
        this.setState({ store: configureAppStore(JSON.parse(completeStore)) })
      } else {
        this.setState({ store: store })
      }
      this.setState({ isStoreLoading: false })
    }).catch((error) => {
      // TODO Handle error?
      this.setState({ store: store })
      this.setState({ isStoreLoading: false })
    })
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange.bind(this))
  }

  _handleAppStateChange(currentAppState) {
    AsyncStorage.setItem('completeStore', JSON.stringify(this.state.store.getState()))
  }

  render() {
    if (this.state.isStoreLoading) {
      return <Text>Loading Store ...</Text>
    } else {
      return (
        <Provider store={this.state.store}>
          <DrawerNavigator />
        </Provider>
      )
    }
  }
}

export default App

