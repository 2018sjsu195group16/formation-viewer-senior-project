import { createSlice } from 'redux-starter-kit'

const formationSlice = createSlice({
  initialState: {
    fileJsonString: '',
    formationLists: [],
    locations: [
      {
        id: 'football',
        title: 'U.S. Football Field',
        imageRef: '',
        lengthInMeters: 109.728,
        widthInMeters: 48.768,
      },
      {
        id: 'basketball',
        title: 'Basketball Court',
        imageRef: '',
        lengthInMeters: 28.6512,
        widthInMeters: 15.24,
      },
    ],
    selectedFormationListId: -1,
    selectedLocationId: '',
    selectedCoordinateId: '',
  },
  reducers: {
    addFormationList(state, action) {
      // prevent adding formation list more than once (array.includes() did not work)
      foundIndex = state.formationLists.findIndex((item) => {
        return item.id.toString() === action.payload.formationList.id.toString()
      })

      if (foundIndex === -1) {
        state.formationLists.push(action.payload.formationList)
      }
    },
    renameFormationList(state, action) {
      indexToChange = state.formationLists.findIndex((item) => {
        return item.id.toString() === action.payload.id.toString()
      })

      if (indexToChange !== -1) {
        state.formationLists[indexToChange].title = action.payload.title
      }
    },
    deleteFormationList(state, action) {
      state.formationLists = state.formationLists.filter(item => {
        if (item.id.toString() === action.payload.id.toString()) {
          return false
        }
        return true
      })
    },
    selectFormationList(state, action) {
      state.selectedFormationListId = action.payload.id
      // TODO verification somewhere that is proper JSON
      state.fileJsonString = JSON.stringify(state.formationLists[action.payload.id])
    },
    selectLocation(state, action) {
      state.selectedLocationId = action.payload.id
    },
    setFileJsonString(state, action) {
      // TODO file verification
      state.fileJsonString = action.payload.fileJsonString
    },
    selectCoordinateId(state, action) {
      state.selectedCoordinateId = action.payload.id
    },
  }
})

// Exports

// Extract and export the action creators object and the reducer
export const { actions, reducer } = formationSlice

// Extract and export each action creator by name
export const { addFormationList, renameFormationList, deleteFormationList, selectFormationList, selectLocation, selectCoordinateId, } = actions

// Export the reducer as default
export default reducer
