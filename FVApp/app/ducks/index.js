import { configureStore, getDefaultMiddleware } from 'redux-starter-kit'
import { combineReducers } from 'redux'

// import reducer slices
import appSettingsReducer from './appSettings'
import formationReducer from './formation'

// combine reducers
rootReducer = combineReducers({
  appSettings: appSettingsReducer,
  formation: formationReducer,
})

export default function configureAppStore(preloadedState) {
  const store = configureStore({
    reducer: rootReducer,
    middleware: [...getDefaultMiddleware()],
    preloadedState,
    enhancers: []
  })

  if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./ducks', () => store.replaceReducer(rootReducer))
  }

  return store
}

export const reducer = rootReducer
