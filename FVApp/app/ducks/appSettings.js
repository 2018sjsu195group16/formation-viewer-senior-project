import { createSlice } from "redux-starter-kit";

const appSettingsSlice = createSlice({
  initialState: {
    color: "white",
    welcomeText: "Welcome to React Native!"
  },
  reducers: {
    // example reducer
    setSnowmmanColor(state, action) {
      // Reducer code goes here (use Immer)
      state.color = action.payload;
    },
    setWelcomeText(state, action) {
      state.welcomeText = action.payload;
    }
  }
});

// console.log(appSettingsSlice);
/*
{
    actions : {
        setSnowmmanColor
    },
    reducer
}
*/

// console.log(setSnowmmanColor({ color: "green" }));
// {type : "setSnowmmanColor", payload : {color : "green"}}

// Exports

// Extract and export the action creators object and the reducer
export const { actions, reducer } = appSettingsSlice;

// Extract and export each action creator by name
export const { setSnowmmanColor, setWelcomeText } = actions;

// Export the reducer as default
export default reducer;
