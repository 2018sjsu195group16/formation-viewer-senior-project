Formations are fixed locations at a given point in a routine or sports play. 
Group-based physical activities such as sports, theater, and dance, use 
formations to strategize and organize themselves. A large amount of time and 
preparation goes into learning and mastering formations individually. A 
group’s performance quality is reliant on each person’s sensitivity to their 
group members. Mastery of formations and group sensitivity results in artistry.

Group-based physical activities require the members to physically attend and 
it is usually difficult to coordinate everyone’s schedules. Sometimes, people 
miss practice or want extra practice. Group time can be spent on helping 
individuals; however, there is not enough of it to spend on every member to 
master the formations. There is a need to help people to practice on their 
own and cultivate sensitivity outside of group practices.

This project will allow users to practice group formations with or 
without their group members. The application combines the user’s practice 
environment and the formations they have to learn using augmented reality 
(AR). The application can simulate group members, so any number of 
members can practice without having the whole group. Thus, artistry born 
from mastery and sensitivity improves the group’s performance.


